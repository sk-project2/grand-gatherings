import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { NgToastService } from 'ng-angular-popup';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent implements OnInit {
  User: any;
  EmailId:any;
  siteKey: string;
  constructor(private router: Router, private service: UserService, private toast: NgToastService) {
    this.siteKey = '6Ld-dUEpAAAAAPPZenwaim_U_JK7_OcKEjJ76OuY'
  }
  ngOnInit() {

  }
  async submitLogin(loginForm: any) {
    this.User = null;
    localStorage.setItem("EmailId", loginForm.emailId);

    if (loginForm.emailId === "admin" && loginForm.password === "admin@123") {
      this.service.setLoginStatus();
      this.router.navigate(["about-us"])
      this.toast.success({ detail: "Success Message", summary: "Login in successful", duration: 5000, sticky: true, position: 'topCenter' })

    } else {
      await this.service.userLogin(loginForm.emailId, loginForm.password).then((data: any) => {
        this.User = data;
      });
      if (this.User != null) {
        this.service.setLoginStatus();
        this.router.navigate(["homepage"]);
        this.toast.success({ detail: "Success Message", summary: "Login in successful", duration: 5000, sticky: true, position: 'topCenter' })
      }else{
      this.toast.error({ detail: "Error Message", summary: "Invalid Credentials", duration: 5000, sticky: true, position: 'topCenter' })
    }
  }
  }

}


