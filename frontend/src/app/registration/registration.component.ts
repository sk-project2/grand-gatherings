import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import {  Router } from '@angular/router';
import { UserService } from '../user.service'
import { NgToastComponent, NgToastService } from 'ng-angular-popup';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  registerForm = this.fb.group({
    firstNaneName: ['', [Validators.required]],
    lastName:['',Validators.required],
    phoneNumber: [' ', Validators.required],
    emailId: ['', [Validators.required, Validators.email]],
    password: ['', Validators.required],
    confirmPassword: ['', Validators.required],
  });

  User:any
  passwordptn='^(?=.?[A-Z])(?=.?[a-z])(?=.*?[0-9]).{8,12}$'
  registerArray:any={}; 
  Users: any;
  rege:boolean=true;

  constructor(private router: Router,private service: UserService,private fb: FormBuilder,private toast:NgToastService){
    this.User = {
      firstName: '',
      lastName: '',
      phoneNumber: '',
      emailId: '',
      password: '',
      conformPassword:'',

  }
}

  ngOnInit() : void{
  }
  login() {
     

  }

  register(regForm:any){  

    this.User.firstName = regForm.firstName;
    this.User.lastName = regForm.lastName;
    this.User.phoneNumber = regForm.phoneNumber;
    this.User.emailId = regForm.emailId;
    this.User.password = regForm.password;
    this.User.conformPassword = regForm.conformPassword;

    console.log(this.User);

    this.service.registerUser(this.User).subscribe((data: any) => {
      this.toast.success({ detail: "Success Message", summary: "Registration successful", duration: 5000, sticky: true, position: 'topCenter' })
      console.log(data);
      this.router.navigate(["login"]);
      
    });

}
}
