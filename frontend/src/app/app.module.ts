import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { NgxCaptchaModule } from 'ngx-captcha';
import { HttpClientModule } from '@angular/common/http';
import { NgToastModule } from 'ng-angular-popup';
// import { ProductsComponent } from './products/products.component';
import { HomepageComponent } from './homepage/homepage.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { FooterComponent } from './footer/footer.component';
import { TermsComponent } from './terms/terms.component';
import { RefundsComponent } from './refunds/refunds.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ContactusComponent } from './contactus/contactus.component';
import { LogoutComponent } from './logout/logout.component';
import { WeddingComponent } from './wedding/wedding.component';
import { SocialComponent } from './social/social.component';
import { CorporateeventsComponent } from './corporateevents/corporateevents.component';
import { EventsComponent } from './events/events.component';





@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    NavBarComponent,
    EventsComponent,
    // ProductsComponent,
    HomepageComponent,
    AboutUsComponent,
    FooterComponent,
    TermsComponent,
    RefundsComponent,
    PrivacyComponent,
    ContactusComponent,
    LogoutComponent,
    WeddingComponent,
    SocialComponent,
    CorporateeventsComponent,
    EventsComponent,
    

   
    
  
    
    
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    NgxCaptchaModule,
    HttpClientModule,
    NgToastModule,
    CarouselModule.forRoot(),
    NgbModule,
    
  ],
   
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

