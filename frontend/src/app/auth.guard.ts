import { CanActivateFn } from '@angular/router';
import { Inject } from '@angular/core';
import { UserService } from './user.service';
export const authGuard: CanActivateFn = (route, state) => {
  let service = Inject(UserService);
  return service.getLoginStatus();
};
